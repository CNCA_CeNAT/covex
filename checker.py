import os

def check_file_existence(file_path, file_type):
    if not os.path.exists(file_path):
        print(f"Error: {file_type} file '{file_path}' does not exist.")
        return False
    return True

def check_folder_existence(folder_path, folder_name):
    if not os.path.exists(folder_path):
        print(f"Error: {folder_name} folder '{folder_path}' does not exist.")
        return False
    return True
