import subprocess
import os

def update_pangolin():
    print("Updating Pangolin version")

    # Check if 'covex' conda environment is activated
    activated_env = subprocess.getoutput("conda info --envs")
    if "covex" not in activated_env:
        print("Error: 'covex' environment is not activated. Please activate it before running this script.")
        return

    # Download the latest Pangolin release from GitHub
    release_url = (
        subprocess.getoutput(
            "wget -qO- https://api.github.com/repos/cov-lineages/pangolin/releases"
        )
        .split('"tarball_url": "')[1]
        .split('"')[0]
    )

    # Update Pangolin in the 'covex' environment
    subprocess.run(["pip", "uninstall", "pangolin", "--yes"])  
    subprocess.run(["pip", "install", release_url]) 

    print("Pangolin version updated successfully in the 'covex' environment")

def update_nextclade():
    print("Updating Nextclade version")

    # Check if 'covex' conda environment is activated
    activated_env = subprocess.getoutput("conda info --envs")
    if "covex" not in activated_env:
        print("Error: 'covex' environment is not activated. Please activate it before running this script.")
        return

    # Remove Nextclade with Conda
    subprocess.run(["conda", "remove", "--name", "covex", "nextclade", "--yes", "-q", "--force"])  

    # Install the latest version of Nextclade from the 'bioconda' channel
    subprocess.run(["conda", "install", "-c", "bioconda", "--name", "covex", "nextclade", "--yes", "-q"])  

    print("Nextclade version updated successfully in the 'covex' environment")

if __name__ == '__main__':
    update_pangolin()
    update_nextclade()


