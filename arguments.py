import argparse
from update_pangolin import update_pangolin, update_nextclade
import sys 

def get_args():
    parser = argparse.ArgumentParser(prog='CoVEx', 
                                     description='CoVEx takes raw sequences data to analyze SARS-CoV-2 mutation')

    # Define the arguments for 'covex'
    parser.add_argument('-i', '--input', required=False, 
                        help='Input folder containing fastq files (Required argument)')
    parser.add_argument('-o', '--output', required=False, 
                        help="Output folder for CoVEx results (Required argument)")
    parser.add_argument('-r', '--ref', required=False, 
                        help="Path to genome reference sequence (Required argument)")
    parser.add_argument('-b', '--bed', required=False, help="Path to primer.bed file (Optional argument)")
    parser.add_argument('-m', '--metadata', required=False, help="Path to metadata file (Optional argument)")

    # Define the optional flag for 'update_pangolin' and 'update_nextclade'
    parser.add_argument('--update_pangolin', action='store_true', help="Update Pangolin")
    parser.add_argument('--update_nextclade', action='store_true', help="Update Nextclade")

    args = parser.parse_args()

    if args.update_pangolin:
        update_pangolin()
        sys.exit(0)
    elif args.update_nextclade:
        update_nextclade()
        sys.exit(0)
    elif not args.input or not args.output or not args.ref:
        parser.error("The -i/--input, -o/--output, and -r/--ref arguments are required unless --update_pangolin or --update_nextclade is specified.")

    return args







